<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\User;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $datas = [
            [
                'email' => 'fredy@app.com',
                'name' => 'Fredy',
                'password' => \Hash::make('password'),
                'alamat' => 'Jakarta',
                'phone' => '085162728',
                'photo' => 'http://207.148.121.63/avatar.png'
            ],
            [
                'email' => 'surya@app.com',
                'name' => 'Surya Prima',
                'password' => \Hash::make('password'),
                'alamat' => 'Jakarta',
                'phone' => '085162728',
                'photo' => 'http://207.148.121.63/avatar.png'
            ],
            [
                'email' => 'mistra@app.com',
                'name' => 'Mistra',
                'password' => \Hash::make('password'),
                'alamat' => 'Jakarta',
                'phone' => '085162728',
                'photo' => 'http://207.148.121.63/avatar.png'
            ],
            [
                'email' => 'dwi@app.com',
                'name' => 'Dwi Yulianto',
                'password' => \Hash::make('password'),
                'alamat' => 'Jakarta',
                'phone' => '085162728',
                'photo' => 'http://207.148.121.63/avatar.png'
            ],
            [
                'email' => 'anita@app.com',
                'name' => 'Anita',
                'password' => \Hash::make('password'),
                'alamat' => 'Jakarta',
                'phone' => '085162728',
                'photo' => 'http://207.148.121.63/avatar.png'
            ],
            [
                'email' => 'ozan@app.com',
                'name' => 'Ozananda',
                'password' => \Hash::make('password'),
                'alamat' => 'Jakarta',
                'phone' => '085162728',
                'photo' => 'http://207.148.121.63/avatar.png'
            ],
            [
                'email' => 'gilang@app.com',
                'name' => 'Gilang',
                'password' => \Hash::make('password'),
                'alamat' => 'Jakarta',
                'phone' => '085162728',
                'photo' => 'http://207.148.121.63/avatar.png'
            ]
        ];

        foreach($datas as $data){
            User::create($data);
        }
    }
}
