<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class UserController extends Controller
{
    public function upload(Request $request){
        $user_auth = Auth::user();
        $getImage = $request->image;
        $imageName = time().'.'.$getImage->extension();
        $imagePath = public_path(). '/images';

        $getImage->move($imagePath, $imageName);

        $user = User::find($user_auth->id);
        $user->photo = "http://207.148.121.63/images/".$imageName;
        $user->save();

        return response()->json([
            "success" => true,
            "photo" => "http://207.148.121.63/images/".$imageName
        ]);
    }
}
